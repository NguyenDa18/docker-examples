# Deploy an app

+ `kubectl get nodes`
+ 

[hello minikube](https://kubernetes.io/docs/tutorials/hello-minikube/)

## kubectl basics

View the nodes in a cluster

```
kubectl get nodes
```

Create a new deployment.

```
kubectl run kubernetes-bootcamp --image=docker.io/jocatalin/kubernetes-bootcamp:v1 --port=8080
```

List deployments.

```
kubectl get deployments
```

To view the application output without exposing it externally, create a route between the terminal and the Kubernetes cluster using a proxy.

```
$ kubectl proxy
```

Get the name of the Pod and store it in the POD_NAME environment variable.

```
$ export POD_NAME=$(kubectl get pods -o go-template --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}')
$ echo Name of the Pod: $POD_NAME
```

In a new terminal send a request to the application using curl.

The one below doesn't work any more.
```
curl http://localhost:8001/api/v1/proxy/namespaces/default/pods/$POD_NAME/
```
I've had better luck with this one.

```
curl http://localhost:8001/api/v1/namespaces/default/pods/
```

List the environment variables.

```
kubectl exec $POD_NAME env
```

Start a bash session in the Pod’s container.


```
kubectl exec -ti $POD_NAME bash
```

We have now an open console on the container where we run our NodeJS application. The source code of the app is in the server.js file:

```
cat server.js
```

You can check that the application is up by running a curl command:


```
kubectl get deployments
```
 
 



# Kubernetes Commands

## Google Cloud
[google cloud console](https://console.cloud.google.com)
+ `gcloud auth configure-docker`

## List local pods
+ `kubectl get pods`

## Get Pod Details
+ `kubectl describe pod <pod>`

## Deploy to K8S
+ `docker tag <image-name> gcr.io/<project-id>/<image>:<tag>`
+ `docker push gcr.io/<project-id>/<image>:<tag>`

### Regions
+ `gcr.io` hosts the images in the United States, but the location may change in the future
+ `us.gcr.io` hosts the image in the United States, in a separate storage bucket from images hosted by gcr.io
+ `eu.gcr.io` hosts the images in the European Union
+ `asia.gcr.io` hosts the images in Asia

### Deployments
+ `gcloud projects list`
+ `kubectl run <image-name> --image=gcr.io/<project-id>/<image-name>:<tag> --port=<port>`
+ `kubectl get deployments`
+ `kubectl delete deploy <deployment>`


### Expose Deployment
+ `kubectl expose deployment/<image-name> --type="NodePort" --port <port>`
+ `kubectl expose deployment <image-name> --type=LoadBalancer --name=<arbitrary name`
+ `kubectl describe services/<image-name>`
+ `export NODE_PORT=$(kubectl get services/<image-name> -o go-template='{{(index .spec.ports 0).nodePort}}')`
+ `echo NODE_PORT=$NODE_PORT`

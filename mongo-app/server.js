const express = require('express');
const app = express();
const mongodb = require('mongodb');
const config = require('./db');
const PORT = 4000;
const client = mongodb.MongoClient;
const bodyParser = require('body-parser');
const ObjectID = require('mongodb').ObjectID

// "Middleware" required to examine body during express post and put processing
//
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

client.connect(config.DB, function(err, db) {
  if (err) throw err;
  var dbo = db.db("mydb");
  dbo.createCollection("customers", function(err, res) {
    if (err) throw err;
    console.log("Collection created!");
    db.close();
  });
});

const mongoose = require('mongoose');
mongoose.connect(config.DB);


app.get('/', function(req, res) {
    res.json({"hello": "world"});
});

app.put('/create', function(req, res) {
    
  var objectId = new ObjectID();
  console.log('********************** ', req.body)
    
  client.connect(config.DB, function(err, db) {
    if (err) throw err;
    var dbo = db.db("mydb");
    var myobj = req.body;
    dbo.collection("customers").insertOne(myobj, function(err, res) {
      if (err) throw err;
      console.log("1 document inserted");
      db.close();
    })
  })
  res.send(req.body);
});

app.listen(PORT, function(){
    console.log('Your node js server is running on PORT:',PORT);
});

<?php
//this is so that report.php won't send an email off unless it is deliberately included, rather than run from a browser
if($runreport)
{

require_once 'settings.php';
require_once 'functions.php';



$minvalue=2;
$fontsize=4;

$stats='browser';
$charttype='pie';

function checkdatestuff($test,$extra)
{if(is_numeric($test)){return $test;}
else{switch($extra)
{case 'year';return date("Y");break;
default;return 0;break;}}}

$dbtill=time();

$timespan='specific';

$dbfrom=mktime(0, 0, 0, date("n")-1,1, date("Y"), -1);//last bit set to -1 so PHP tries to do daylight saving time by itself.
$dbtill=mktime(0, 0, 0, date("n"),1, date("Y"), -1);
$specifictitle=date('F Y',$dbfrom);



$countby=2;

switch($countby)
{case 1;$echocount="Hit";break;
case 2;$echocount="Unique Hit";break;
case 3;$echocount="IP";break;
default;$echocount='Default';$countby=$howtocount;break;}

$browser=array();
$allbrowsers=array();
$browserscount=array();
$browsers=array();

//$browser=unserialize(stripslashes(eregi_replace('\\','',$_GET['browser'])));
$browser=unserialize(stripslashes($_GET['browser']));
if (!is_array($browser)){$browser=array();}
if(in_array('Other', $browser)){$others=true;}

$total=0;
$ips=array();
$uniqueips=array();
$totalhits=0;

$versions=array();
$allversions=array();
$versionscount=array();

$OSversions=array();
$allOSversions=array();
$OSversionscount=array();

$times=array('0000','0100','0200','0300','0400','0500','0600','0700','0800','0900','1000','1100','1200','1300','1400','1500','1600','1700','1800','1900','2000','2100','2200','2300');
$timescount=array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

$con = mysql_connect($servername, $dbusername, $dbpassword);
if (!$con){die("Could not connect:" . mysql_error());}
mysql_select_db($dbname, $con);

switch($countby)
{case 2;//unique hits - will require all data
$result = mysql_query("SELECT * FROM $dbtablename");
break;
default;
$result = mysql_query("SELECT * FROM $dbtablename WHERE time > '".mysql_real_escape_string($dbfrom)."' AND time < '".mysql_real_escape_string($dbtill)."'");
break;}


while($row = mysql_fetch_array($result))
{
$continue=true;

if($countby==2)
{//stats by UNIQUE IPs

if(!in_array($row['ip'], $uniqueips)){$uniqueips[]=$row['ip'];}else{$continue=false;}

if($row['time']<$dbfrom || $row['time']>$dbtill){$continue=false;}
else{
$totalhits++;
//if it's in the correct time, get data for different IPs
if(!in_array($row['ip'], $ips)){$ips[]=$row['ip'];}
}

}else{
//stats for different IPs or hits
$totalhits++;
//checks to see if IP is in the ips array, if not adds it.  if it is then decides whether to continue or not.
if(!in_array($row['ip'], $ips)){$ips[]=$row['ip'];}else{switch($countby){case 3;case 2;$continue=false;break;}}
}

if($_GET['stats']=='version')
{
switch($others)
{
case true;
if(in_array($row['browser'], $browser)){$continue=false;}
break;
default;
if(!in_array($row['browser'], $browser)){$continue=false;}
break;
}
}


if($continue==true)
{
$total++;
if(!in_array($row['browser'].' '.$row['version'], $versions)){$versions[]=$row['browser'].' '.$row['version'];}
$allversions[]=$row['browser'].' '.$row['version'];

if(!in_array($row['browser'], $browsers)){$browsers[]=$row['browser'];}
$allbrowsers[]=$row['browser'];

if(!in_array($row['os'], $OSversions)){$OSversions[]=$row['os'];}
$allOSversions[]=$row['os'];

$timescount[tohours($row['time'])]++;

//end of continue if statement
}
//end of while loop
}

//this works out the quantity of different versions, and then the number of each version.  I was quite impressed I managed to think it though, but I can't remember how it works anymore
for ($i=0;$i<sizeof($versions);$i++)
{
$versionscount[$i]=0;
for ($i2=0;$i2<sizeof($allversions);$i2++)
{if($allversions[$i2]==$versions[$i]){$versionscount[$i]++;}}
}

for ($i=0;$i<sizeof($browsers);$i++)
{
$browserscount[$i]=0;
for ($i2=0;$i2<sizeof($allbrowsers);$i2++)
{if($allbrowsers[$i2]==$browsers[$i]){$browserscount[$i]++;}}
}

for ($i=0;$i<sizeof($OSversions);$i++)
{
$OSversionscount[$i]=0;
for ($i2=0;$i2<sizeof($allOSversions);$i2++)
{if($allOSversions[$i2]==$OSversions[$i]){$OSversionscount[$i]++;}}
}



switch($stats)
{
case 'browser';
$options=$browsers;
$values=$browserscount;
$title="Browsers by ".$echocount;
break;
case 'version';
$options=$versions;
$values=$versionscount;

switch($others)
{
case true;
$versionsecho='Other ';
break;
default;
$versionsecho='';
for($i=0;$i<sizeof($browser);$i++){$versionsecho.=$browser[$i].', ';}
break;
}

$title="Versions of ".$versionsecho."by ".$echocount;
break;
case 'os';
$options=$OSversions;
$values=$OSversionscount;
$title="Platforms by ".$echocount;
break;
case 'time';
$options=$times;
$values=$timescount;
$title="Visit Time (24hrs GMT) by ".$echocount;
break;

default;
$options=unserialize($_GET['options']);
$values=unserialize($_GET['values']);
$total=$_GET['total'];
break;
}


if($timespan!=='date' && $timespan!=='specific' && $_GET['till']!=='date')
{
if($timespan!=='ever'){
$title.=" for the last $timespan";
}
else{
$title.=" for all data";
}
}
elseif($timespan=='specific')
{
$title.=" for $specifictitle";
}else{
$title.=" from ".date('d/m/y',$dbfrom)." til ".date('d/m/y',$dbtill);

}


//looks for all the options with less than the minimum percentage of values, and lumps them all into 'other'

$thingsinother=array();

$othervalue=0;
for($i=0;$i<sizeof($values);$i++)
{
if($total!==0){$checkthis2=round($values[$i]/$total*100);}
else{$checkthis2=0;}
if($checkthis2<$minvalue){$othervalue+=$values[$i];$thingsinother[]=$i;}
}

if($othervalue>0 && count($thingsinother)>1)
{
$options[]="Other";
$values[]=$othervalue;
}


if($stats!=='time')
{
//only sort into order if not times of day
//orders arrays.
$maxvalue=0;
$valueschecked=0;
$tempi=0;
$newvalues=array();
$newoptions=array();
$i=0;
$oldvalues=$values;
//continues until the arrays have been completely reshuffled
while($i<sizeof($values))
{
$maxvalue=0;
for($i2=0;$i2<sizeof($values);$i2++)
{//this searches through and finds the largest number in the values array
if($values[$i2]>$maxvalue){$maxvalue=$values[$i2];$tempi=$i2;}
}
//deletes this number from the values array, but puts it, and it's corrosponding option into the newoptions and newvalues arrays

if(round($values[$tempi]/$total*100)>=$minvalue || $options[$tempi]=='Other' || count($thingsinother)<=1)
{//if the percentage is below the minimum, they've been lumped into 'other' so don't add them
$newvalues[]=$values[$tempi];
$newoptions[]=$options[$tempi];
}
$values[$tempi]=0;

$i++;
}

$rowheight=25;
$rowspacer=3;

//end of if stats!==time
}else{
$newvalues=$values;
$newoptions=$options;

$rowheight=18;
$rowspacer=1;

}


switch($charttype)
{
case 'pie';
//error_reporting(E_ALL);
require 'piechart.php';

$pie=new piechart();
$pie->settitle($title);
$pie->setsize(350);
$pie->setdata($newoptions,$newvalues);
$pie->setsave('tempimage.png');

$extrainfo="Total: ".$total."\n ";

if($othervalue>1)
{
$extrainfo.="\nOther:";
for($i=0;$i<count($thingsinother);$i++)
{
$extrainfo.="\n".$options[$thingsinother[$i]]." (".$oldvalues[$thingsinother[$i]].")";
}
$pie->setinfo($extrainfo);
}

$pie->drawchart();

break;
default;//draw bar chart

//distance from left the bars should start (number of characters in largest name)
$maxlength=0;
for($i=0;$i<sizeof($newoptions);$i++)
{
if(strlen($newoptions[$i])>$maxlength){$maxlength=strlen($newoptions[$i]);}
}


if($total==0){$maxwidth=$maxlength*9+2+100;}
else
{$maxwidth=$maxlength*9+2+round($newvalues[0]/$total*300)+100;}
if(strlen($title)*8>$maxwidth){$maxwidth=strlen($title)*8;}

$maxheight=(sizeof($newoptions)+1)*$rowheight;

$im = imageCreate($maxwidth,$maxheight);
$background = imageColorAllocate($im, 255, 255, 255);
imagecolortransparent($im,$background);
$black = imageColorAllocate ($im, 0, 0, 0); 
$red = imageColorAllocate ($im, 255, 0, 0);
$yellow = imageColorAllocate ($im, 255, 255, 0);
$green = imageColorAllocate ($im, 0, 255, 0);
$blue = imageColorAllocate ($im, 0, 0, 255);




imagestring($im, $fontsize, 0, 0 , $title,  $black);

for($i=0;$i<sizeof($newoptions);$i++)
{
$i2=$i+1;
switch (fmod($i, 4))
{
case 0;$colour=$red;break;
case 1;$colour=$yellow;break;
case 2;$colour=$green;break;
default;$colour=$blue;break;
}
//option's string
imagestring($im, $fontsize, 0, $i2*$rowheight+$rowspacer , $newoptions[$i],  $black);
//bar for length of option's value
imagefilledrectangle($im, $maxlength*9+2, $i2*$rowheight+$rowspacer, $maxlength*9+2+round($newvalues[$i]/$total*300), (($i2+1)*$rowheight)-$rowspacer,$colour);
//text for option's value
imagestring($im, $fontsize, $maxlength*9+2+round($newvalues[$i]/$total*300)+5, $i2*$rowheight+$rowspacer , $newvalues[$i].'='.round($newvalues[$i]/$total*100).'%',  $black);
}

//header('Content-type: image/png');
imagePNG($im,'tempimage.png');
imageDestroy($im);

break;}



//email stuff

if($usephpmailer)//use PHPmailer script
{


include("class.phpmailer.php");
include("class.smtp.php");

$mail=new PHPMailer();

/*
//if set to true, configure below:
$smtpAuth   = true;                  // enable SMTP authentication
$smtpSecure = "ssl";                 // sets the prefix to the servier
$smtpHost   = "smtp.gmail.com";      // sets the SMTP server
$smtpPort   = 465;                   // set the SMTP port - default set for gmail.
$smtpUsername = "luke.wallin";  // SMTP username
$smtpPassword = "Computing23";  // SMTP password
*/

$mail->IsSMTP();
$mail->SMTPAuth   = $smtpAuth;                  // enable SMTP authentication
$mail->SMTPSecure = $smtpSecure;                 // sets the prefix to the servier
$mail->Host       = $smtpHost;      // sets GMAIL as the SMTP server
$mail->Port       = $smtpPort;                   // set the SMTP port 

$mail->Username   = $smtpUsername;  // GMAIL username
$mail->Password   = $smtpPassword;            // GMAIL password

$mail->From       = $fromemail;
$mail->FromName   = "Stat Counter";
$mail->Subject    = "Website statistics for the last Month.";
$mail->Body       = 'Hello!<p> It is now '.date('F').' and last month, '.date('F',mktime(date('H'), date('i'), date('s'), date("n")-1,date('d'),date("Y"),-1)).', your website '.$sitename." had $totalhits hits, of which ".count($ips)." were different IPs and $total were unique hits. \n"; //HTML Body
$mail->AltBody    = 'Hello! It is now '.date('F').' and last month, '.date('F',mktime(date('H'), date('i'), date('s'), date("n")-1,date('d'),date("Y"),-1)).', your website '.$sitename." had $totalhits hits, of which ".count($ips)." were different IPs and $total were unique hits. \n"; //Text Body

//$mail->WordWrap   = 50; // set word wrap

$mail->AddAddress($myemail,$emailname);
$mail->AddReplyTo($fromemail,"Webmaster");
$mail->AddAttachment("tempimage.png");             // attachment
//$mail->AddAttachment("/path/to/image.jpg", "new.jpg"); // attachment

$mail->IsHTML(true); // send as HTML

if(!$mail->Send()) {
  echo "Mailer Error: " . $mail->ErrorInfo;
} else {
  echo "Message has been sent";
}


}
else//use mail()
{
$headers = "From: $fromemail \r\n" .
				   "Reply-To: $fromemail \r\n" .
					 'X-Mailer: PHP/' . phpversion();
$headers .= "MIME-Version: 1.0\n";
$boundary = '-----=' . md5( uniqid ( rand() ) );
$headers .= "Content-Type: multipart/mixed; boundary=\"$boundary\"";
$message = "--" . $boundary . "\n";
$message .='Content-Type: text/html; charset="iso-8859-1"' . "\n";
$message.='Content-Transfer-Encoding: 7bit' . "\r\n\r\n";
$message.='Hello! It is now '.date('F').' and last month, '.date('F',mktime(date('H'), date('i'), date('s'), date("n")-1,date('d'),date("Y"),-1)).', your website '.$sitename." had $totalhits hits, of which ".count($ips)." were different IPs and $total were unique hits. \n";
$message .= "--" . $boundary . "\n";


$message .= "Content-Type: image/png; name=\"statistics\"\n";
$message .= "Content-Transfer-Encoding: base64\n";
$message .= "Content-Disposition: attachment; filename=\"stats.png\"\r\n\r\n";
//$message .= "Content-Disposition: inline; filename=\"stats.png\"\r\n\r\n";
$content=file_get_contents('tempimage.png');
$content_encode = chunk_split(base64_encode($content));
$message .= $content_encode . "\n";
$message .= "--" . $boundary . "\n";


mail($myemail, 'Website statistics for the last Month.', $message, $headers);
}


unlink('tempimage.png');





//end of it statement
}



?>

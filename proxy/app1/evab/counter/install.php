<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
<title>Installation</title>
</head>
<body>
<h1>Step One</h1>
Edit settings.php so that  
<pre>
$servername
$dbusername
$dbpassword
</pre>
allow PHP to connect to your MySQL database.  For example:
<pre>
$servername='localhost';
$dbusername='Bobby';
$dbpassword='bobspassword';
</pre>

You will also want to change the settings for monthly emails, whether to have them at all, and if you want them to put your email address in the right place.<br><br>

Then set
<pre>
$dbname
</pre>
to the name of the database and table you want the stat counter to use (has to already exist) For Example:
<pre>
$dbname='website';
$dbtablename='statcounter';
</pre>

Once you have done this, you can <a href="install2.php">INSTALL</a>.
</body>
</html>

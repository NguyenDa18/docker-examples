<?php
//pie chart creator version 0.3. Copyright Luke Wallin 2008 - uses anti aliased arc script from http://mierendo.com/software/antialiased_arcs/
//produces a PNG image of a pie chart with a table of data.
//currently this DOESN'T sort the data in any way, since it was written to be embedded in my hit counter script - which already orders the data.
//also it doens't cope with long names very well yet. 
require 'imageSmoothArc.php';

class piechart
{
var $options=array();
var $values=array();
var $title="Pie Chart";
var $size=500;
var $fontsize=20;
//var $font = 'C:\WINDOWS\Fonts\arial.ttf';
var $font="Vera.ttf"; 
var $extrainfo='';
//set to a filename if you want to save, rather than view, the PNG.
var $saveimg=false;


function setdata($setoptions,$setvalues)
{$this->options=$setoptions;
$this->values=$setvalues;}

function settitle($setto)
{$this->title=$setto;}

function setinfo($setto)
{$this->extrainfo=$setto;}

function setsave($setto)
{$this->saveimg=$setto;}

function setfont($setto)
{$this->font=$setto;}

function setsize($setto)
{$this->size=$setto;
$this->fontsize=max(round($setto/20),12);}

function drawchart()
{

$total=0;

$longestname=0;
$largestvalue=0;
$bigoption=0;
//find total values and longest name
for($i=0;$i<count($this->values);$i++)
{$total+=$this->values[$i];
if($this->values[$i]>$largestvalue){$largestvalue=$this->values[$i];}
$textsize=imagettfbbox($this->fontsize-5, 0, $this->font, $this->options[$i]);//." ".round($this->values[$i]*100/$total)."%"
$templengthoftext=$textsize[2]-$textsize[0];
if($templengthoftext>$longestname){$longestname=$templengthoftext;$bigoption=$i;}
}

//$textsize=imagettfbbox($this->fontsize-5, -$midangle-$angleoftextdeg-$upsidedown, $this->font, $this->options[$bigoption]);//." ".round($this->values[$i]*100/$total)."%"
//$longestname=sqrt( pow($textsize[0]-$textsize[2],2) + pow($textsize[1]-$textsize[3],2) );

if($total!==0){$largestvaluepercent=round($largestvalue*100/$total);}
else{$largestvaluepercent=0;}

$textsize=imagettfbbox($this->fontsize-5, 0, $this->font, $largestvalue." = ".$largestvaluepercent."%");//." ".round($this->values[$i]*100/$total)."%"
$longestlengthofnumbers=$textsize[2]-$textsize[0];//sqrt( pow($textsize[0]-$textsize[2],2) + pow($textsize[1]-$textsize[3],2) );


// Set the enviroment variable for GD - this is so it can find the font.
//putenv('GDFONTPATH=' . realpath('.'));


//find height of table of data
$tableheight=count($this->values)*$this->fontsize;

//extra info exists - find its height
if(strlen($this->extrainfo)>0)
{
$textsize=imagettfbbox($this->fontsize-5,0, $this->font, $this->extrainfo);
$infoheight=$textsize[1]-$textsize[7]+$this->fontsize;
$infowidth=$textsize[2]-$textsize[0];
//$infoheight=220;
}else{$infoheight=0;}



$imgwidth=$this->size;
$extrawidth=$longestname+$longestlengthofnumbers+$this->fontsize*2;

//if extra info is too wide, make table wider.
if($infowidth+$this->fontsize*2 > $extrawidth){$extrawidth=$infowidth+$this->fontsize*2;}

$imgheight=$this->size;
$extraheight=$this->fontsize;

if(($infoheight+$tableheight)>($imgheight+$extraheight))
{//if info box is too big.
$extraheight2=$tableheight+$infoheight-$imgheight+$this->fontsize*2;//-$extraheight;
}
else{$extraheight2=0;}
//$im = imageCreate($imgwidth,$imgheight+$this->fontsize);
$im = imageCreateTrueColor($imgwidth+$extrawidth , $imgheight+$extraheight+$extraheight2);

//add transparent background
imagealphablending($im,false);
$transparent = imagecolorallocatealpha($im, 255, 255, 255, 127);
imagefilledrectangle($im,0,0,$imgwidth+$extrawidth , $imgheight+$extraheight+$extraheight2 , $transparent);
imagealphablending($im,true);


$black = imageColorAllocate ($im, 0, 0, 0); 
$red = imageColorAllocate ($im, 200, 0, 0);
$orange = imageColorAllocate ($im, 255, 150, 0);
$yellow = imageColorAllocate ($im, 220, 255, 0);
$green = imageColorAllocate ($im, 0, 220, 0);
$darkgreen = imageColorAllocate ($im, 0, 102, 0);
$blue = imageColorAllocate ($im, 0, 0, 200);
$purple = imageColorAllocate ($im, 150, 0, 150);
$grey = imageColorAllocate ($im, 128, 128, 128);

$titlefontsize=$this->fontsize;
$titlesize=imagettfbbox($titlefontsize, 0,$this->font, $this->title);
$titleheight=$titlesize[3]-$titlesize[5];
$titlewidth=$titlesize[2]-$titlesize[0];

if($titlewidth>($imgwidth+$extrawidth))
{
$titlefontsize-=5;
$titlesize=imagettfbbox($titlefontsize, 0,$this->font, $this->title);
$titlewidth=$titlesize[2]-$titlesize[0];
}
//write title
ImageTTFText($im, $titlefontsize, 0, round(($this->size-$titlewidth+$extrawidth)/2), $titleheight, $black, $this->font, $this->title);







$centrex=round($this->size/2);
$centrey=round($this->size/2)+$extraheight;
$angle=M_PI;//0;//-M_PI/2;///2;
//$angle=0;
//$angle=90;
$angle=M_PI/2-($largestvaluepercent/100)*2*M_PI;
//if($angle<-180){$angle+=360;}
$tabley=$extraheight + $centrey+$extraheight2/2-round(($tableheight+$infoheight)/2);
//$tabley=0;
//bg to table
imagefilledrectangle($im,$imgwidth+$this->fontsize/4-1, $tabley-$this->fontsize*5/4-1, $imgwidth+$extrawidth-$this->fontsize/2+1 , $tabley+$tableheight-$this->fontsize/2+1+$infoheight , $black);
imagefilledrectangle($im,$imgwidth+$this->fontsize/4, $tabley-$this->fontsize*5/4, $imgwidth+$extrawidth-$this->fontsize/2 , $tabley+$tableheight-$this->fontsize/2+$infoheight , $grey);


//$radius=$imgwidth*3/10;
$radius=$imgwidth/2-$longestname-5;
if($total!==0)
{
for($i=0;$i<count($this->values);$i++)
{

$angle2=$angle+$this->values[$i]*2*M_PI/$total;

if(count($this->values)==1){$angle2-=0.001;}
//angle of middle of slice
$midangle=($angle+$angle2)/2;

while($midangle>2*M_PI)
{$midangle-=2*M_PI;}
//get midangles in range so that upside down text check will work
while($midangle<-2*M_PI)
{$midangle+=2*M_PI;}

//angle swept out by edge of text on pie. used to make text line up properly for small slices (-ve for LHS, +ve for RHS)
//angle in radians = arc length (aprox font height) / radius
//should be -5, using -10 to make up for small error.
$angleoftext=(($this->fontsize-10)/($radius));// * (($midangle>M_PI/2 && $midangle<M_PI*3/2)? -1:1); 
//$angleoftextdeg=180*$angleoftextrad/M_PI;


//find colour to use
switch (fmod($i, 7))
{case 0;$colour=array(255,0,0,0);$colour2=$red;break;
case 1;$colour=array(255,255,0,0);$colour2=$yellow;break;
case 2;$colour=array(0,255,0,0);$colour2=$green;break;
case 3;$colour=array(0,0,255,0);$colour2=$blue;break;
case 4;$colour=array(255,150,0,0);$colour2=$orange;break;
case 5;$colour=array(0,102,0,0);$colour2=$darkgreen;break;
default;$colour=array(150,0,150,0);$colour2=$purple;break;}
//final slice is red and next to first slice which is red
if($i==count($this->options)-1 && fmod($i, 7)==0){$colour=array(255,255,0,0);$colour2=$yellow;}

//draw slice
imageSmoothArc ($im, $centrex, $centrey, $radius*2, $radius*2, $colour, $angle, $angle2);


//finds the width (if text were horizontal) of text.  for use when text is upside down to push it outside the pie.
$textsize=imagettfbbox($this->fontsize-5, 0, $this->font, $this->options[$i]);//." ".round($this->values[$i]*100/$total)."%"
$lengthoftext=$textsize[2]-$textsize[0];

$textradius =$radius+$this->fontsize/4;

//add an angle if text would be upside down
//$upsidedown=($midanglerad>M_PI/2 && $midanglerad<M_PI*3/2)? M_PI:0;
if($midangle>M_PI/2 && $midangle<M_PI*3/2)
{
//text would be upside down
$upsidedown=M_PI;
$angleoftext*=-1;
$textradius+=$lengthoftext;//-$this->fontsize;

$upsidedown=M_PI;
}
else
{
$upsidedown=0;
}

//label on pie //-$angleoftext/2
ImageTTFText($im, $this->fontsize-5, ($midangle-$angleoftext/2+$upsidedown)*180/M_PI, $centrex+($textradius)*cos($midangle-$angleoftext/2), $centrey-$textradius*sin($midangle-$angleoftext/2), $black, $this->font, $this->options[$i]);//." ".round($this->values[$i]*100/$total)."%"
//data label in table
ImageTTFText($im, $this->fontsize-5, 0, $imgwidth+$this->fontsize/2, $tabley, $colour2, $this->font, $this->options[$i]);
//data in table
ImageTTFText($im, $this->fontsize-5, 0, $imgwidth+$this->fontsize/2+$longestname, $tabley, $black, $this->font, "  ".$this->values[$i]." = ".round($this->values[$i]*100/$total)."%");


$tabley+=$this->fontsize;
$angle=$angle2;
}
}else{//total DOES equal 0
$textsize=imagettfbbox($this->fontsize, 0, $this->font, 'No Data');//." ".round($this->values[$i]*100/$total)."%"
$lengthoftext=$textsize[2]-$textsize[0];
$heightoftext=$textsize[3]-$textsize[1];

ImageTTFText($im, $this->fontsize-5, 0, $centrex-$lengthoftext/2, $centrey-$heightoftext/2, $black, $this->font, 'No Data');
//$this->extrainfo='No Info';
}
ImageTTFText($im, $this->fontsize-5, 0, $imgwidth+$this->fontsize/2, $tabley+$this->fontsize, $black, $this->font, $this->extrainfo);

imagealphablending($im,false);
imagesavealpha($im, true);


if($this->saveimg===false)
{header('Content-type: image/png');
imagePNG($im);}
else//save image or view image?
{imagePNG($im,$this->saveimg);}

imageDestroy($im); 
}


}
?>

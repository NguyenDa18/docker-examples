1.0.0 (10-6-07)
-Release version. (ie - version I gave to dave :P)

1.0.1 (10-6-07)
-WindowsCE is now included in 'Windows' rather than 'Other' in viewstats.php

1.0.2 (10-6-07)
-new option for displaying hits or number of different IPs in the hitcounter image.

1.0.3 (10-6-07)
-fixed a font bug which had meant font would always be black.
-fixed a bug which didn't meant users who refused session cookies weren't logged at all.
-added anti-caching in hope to stop browsers (IE) from simply caching the image and not logging properly.

1.0.4
-added googlebot detection
-added option to viewstats to allow you to view only the statistics of the unique IP addresses
-added same option to admin's view individual statistics and browser versions
-added ability to see the actual HTTP_USER_AGENT to individal statistics
-added feature which retains your current settings in the form on the viewstats page 

1.0.5
-browserstats.php now lumps certain browsers together automatically.
For example, Phoenix, Firebird, Firefox and Minefile are all counted as versions of Firefox.
It also says what the version's name is, rather than just it's number.
-Statistics now shows time of day that hits were recorded.

1.0.6
-added option to block a set of IP addresses from being logged.
-improved netscape detection, and hopefully made false-netscape detection much more unlikely
-hopefully will now pick up more bots as bots and not netscape or unknown
-added Other as a version which can be viewed in more detail
-added OSs and time of day to browser versions stats.
-added timer to viewstats to see how long it took to generate.

1.1.0
big enough change to warrent new version I think, as it alters settings.php
-you can now choose which table in the database it uses, so you can have two stat counters or more side by side.

1.1.1
-this table name is also used as the session variable base name (again for two counters side by side)
-viewstats now gives more info on how the stats are set to be collected.

1.2.0 (new major version because of large introduction of statsimage.php)
-added statsimage.php, which does the same job as viewstats.php, but is an image.  Not completely implemented yet.

1.2.1
-statsimage.php is now linked to in various areas of viewstats.php, so you can use these links to get images which can be used on your site to easily and quickly show off your statistics

1.2.2
-PSP detected as browser: NetFont and platform: PSP.
-Iceweasel detection added.
-Slurp (Yahoo's bot) detection added

1.2.3
-improved statsimage.php, now shows results in order of size (descending), and lumps together anything with less than 1% into 'other'.
Minimum threshold can be easily changed using min=x in address.  Use min='0' (eg statsimage.php?stats=browser&min='0') to see everything listed.
-viewstats.php now links to images with min='0', as I thought it would be more useful.
-statsimage.php now supports 'time', and this is linked to in viewstats.php

1.3.0 (new major version because of big improvements)
-re-written viewstats.php using new ideas from statsimage.php, so the list of browsers is dynamically created listing, in descending order, only the major browsers/platforms, with minor ones lumped into other.
-also updated browserstats.php to order browser versions in descending order, and improved how it knows which version to look for.
-linked everything to statsimage.php, which now looks almost identical (for better or for worse) to the actual webpages.
-changed pages viewed section of viewstats.php so it's only displayed if it is actually counted (it checks in settings.php) and generates a (slightly dodgey) cumulative frequency graph and shows some statistics on averages.
I plan to add a box and whisker graph sometime.

1.3.1
-fixed average hits per hour
-added time/date to bottom of page.
-added check latest version to admin page.

1.3.2 (cleanup version hopefully - 28/6/07)
-fixed bug/security flaw with login script.  Now logins for different counters on the same server are seperate - a bug introduced when variable database table names were introduced has been fixed.
-fixed hits per hour, so it should now be correct.
-fixed another bug introduced with the muiltple counters which mean pages viewed wasn't counted at all.

1.3.3 (29-6-07)
-fixed bugs with browser array not being declared before it was used in statsimage, viewstats and browserstats

1.3.4 (29-6-07)
-hopefully actually fixed bug still lurking in statsimage

1.3.5 (29-6-07)
-finnally resolved all problems with over-zealous error reporting PHP installations, and solved the mysterious backslashes in the seralized arrays

1.3.6 (3-09-07)
-added detection of the msnbot.

1.4.0 (22-10-07)
-Added $sitename to settings.php, which is used in viewstats.php
-Added AJAX and lots more options to viewstats.php.  You can now view data from a date untill another date, along with the simple options as per previously
-Can view data for certain months

1.4.1 (23-10-07)
-Taken AJAX out again, but still some JavaScript is left.  All pages and statsimage and various links between are working fully with the new options now.

1.4.2 (23-10-07)
-Now an option in settings.php which will result in the script emailing you once a month with some breif statistics.

1.4.3 (27-10-07)
-Added unique IPs to viewing statistics and monthly email.
-Added 'send email' option to admin panel.
-Added default stats viewing mode option to settings.php

1.5.0 (29-10-07)
-Improved (admin's) individualstats.php so a time and date for each hit is given, and you can now view only hits where browser/version/OS = 'Unknown'.
-Added OS version detection, and a script to search through and update existing database entries.
-Added OS version stats page and Os version to statsimage

1.5.1 (29-10-07)
-Googlebot's OS is now detected as Linux version Google.
-Macs now detected as Power PC or Intel (about the best I could do, they don't say much in their user agent)
-A few linux distros are sometimes detected, again, best which could be done on the little which is said in the user agent.
-Script included which goes through current db and updates old entires with new detection

1.5.2 (30-10-07)
-Fixed statsimage and OSstats.php when viewing the statsistics for 'Other'.
-Need to do the same for 'Other' Browsers.

1.5.3 (3-11-07)
-Taken out pages viewed, it wasn't particularily informative.
-Added hits per day to non-month stats as well, but this sorts by day of week rather than date.
-New system (based on quartiles) for the different colours of times of day/day of week.
-statsimage.php now uses the same colour system where appropriate.

1.5.4 (3-11-07)
-Fixed bug where hits per day of a month were all shifted one day ahead.
-statsimage for hits per day of a month is now smaller, in the same style as hits per hour.
-Pages viewed returned and is now optional (set in settings.php).

1.5.5 (26-02-08)
-General fixes to allow counter script to work error-free:
	-addhit function now uses new browser(FALSE), and checkdatestuff(1,2)
	-session_start() in viewstats called first
	-division by zero in report.php, viewstats.php, statsimage.php if no hits in time period viewed
	-switched str_ireplace with eregi_replace (now PHP 4 compatible)
	-got rid of taking backwards slashes out of GET vars - may or may not be a good move.

1.5.6 (4-03-08)
-Added $sitelink to settings.php which is used for a link at the top of viewstats.php

1.6.0 (26-03-08)
-Added phpmailer for emails (optional). Therefore mail() doesn't have to be used.  Requires openSSL for secure login to smtp server.

1.7.0 (11-05-08)
-Altered database (hence version 1.7.x) to include a unique column.  Addhit checks to see if IP has been seen before and assigns the correct unique value.  This is clearing the way for a cookie check - if a cookie is present even if the IP is unknown set to not unique.
-Above changes made to viewstats, browserstats, OSstats and statsimage.php
-Nice side effect that viewing data by unique is now faster than by different IP (MUCH faster).
-Also optimised counter.php so it shows the number of hits with virtually nil CPU usage.
-Script to update database written.  Adds new column and finds unique IPs.

1.7.1 (12-05-08)
-countby cookies option added - adds a new row to the database same as always, but even if it's a new IP, it will NOt count as unique if the cookie is found!
-countersilent.php is almost certainly broken due to new updates.

1.7.2 (12-05-08)
-viewstats.php now lists uniquehits, diffips, and total hits whatever you're viewing, but bolds the relevent bit.

1.7.3 (15-05-8)
-Pie charts added to statsimage - going to be properly integrated soon
-FIXED: can't have only one 'other' value any more.
-Added links to pie and/or bar charts where apropriate.
-Improved statsimage.php for Versions of Browser (taken browser name out of string for each version - now only a number for pie charts.  Still name + number for bar charts)
-Updated report.php so it's pretty much the same as statsimage.php and sends a pie chart with the email instead of a bar chart. 
-countersilent.php fixed.
-Added size GET var to statsimage to control size of pie charts (default=350)
-Set a max size at 600

1.7.4 (17-5-08)
-Changed 1.7.x update script so it needs you to be logged in (older update scripts already need this).
-Newer version of piechart - more colours
-Added 'total' to piechart

1.7.5 (17-5-08)
-Added Vera font to counter, using it in the same directory - hopefully this will work on any server.

1.7.6 (18-05-08)
-Corrected title of OSstats
-FIX: total of hits is always displayed on statcounter's pie chart - not just when there is an 'other'.

1.7.7 (29-05-08)
-added detection of PS3 (inc. version), Redhat, Wget, iPhone and ipod.
-Added script to update whole DB using latest browser detection script (uses recorded useragents).
-Added '% of total' to Osstats and browserstats pages.

1.7.8 (7-06-08)
-Fixed version number for PS3 not being detected.
-fixed bugs with detection of Wget versions
-useragent is now a TEXT field in the database.
-msnbot, googlebot, slurp and charlote and now browser 'bot' and version (eg) 'googlebot2.1'.  Hopefully this makes the statsistics somewhat more useful, as it quickly identifies non-human traffic.
-Changed behaviour so a blank version will be changed to 'unknown'.

Need to fix: in OStats when viewing 'other' you can click on each OS and view that seperatly.  this doesn't work for borwserstats  Also '% of total' doesn't work after following the link.
Need to add: Mandrivia, archos?, gentoo, PSP version num

1.7.8.1 (30-06-08)
-fixed typo of 'Y' in date in viewstats.php
-using latest version of piechart script (copes with no data, better alignment of text).

1.7.9 (2-08-08)
-Added 'Total: ' to report email statsimage.
-Added security to updateDB_with_latest_browsers.php

1.7.9.1 (03/09/2008)
-added detection of Google Chrome

1.7.9.2 (19/12/2008)
-uses latest version of piechart class (and now works in PHP safemode)

1.7.9.3 (28/01/2009)
-can now detect Windows 7 (tested using beta - Win7 calls itself NT 6.1)

1.7.9.4 (31/01/2009)
-fixed bug with install script, in that if your table wasn't called statcounter it would fail to add the unique column.
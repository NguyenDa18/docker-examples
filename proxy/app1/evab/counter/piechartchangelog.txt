0.01 (15-05-08)
- Had option to switch between true type fonts and not - taken out below

0.02 (15-05-08)
- data, title, fontsize, and imagesize can all be set
- will draw simple pie chart with labels at angle outside of chart
- detects if writing would be upside down and corrects it.
- no anti-aliasing.

0.03 (15-05-08)
- found anti-aliased arc script, and with a bit of tinkering it works.  Angles are a bit of a mess now, being converted to and from radians left right and centre (text, old drawarc and new drawarc all use angle differently).
-Added table of data

0.04 (15-05-08)
- added extrainfo 
-red can't be next to red anymore

idea - abreivate names over 8 characters?
found problem when first angle !==-90 - text goes wrong ways up.  needs looking into. - FIXED

0.05 (16-05-08)
-Tidied up how angles work.  they're now all in radians from x axis, positive going anti-clockwise.  They are only converted to degrees when they need to be.
-As a result of tidy up, the start angle can be anywhere and the text will always be the correct way up.
-Added simple fix for titles which are too long.
-Added option to save image.
-fixed problems when only one peice of data was given.


FOUND BUG - when there's only one option; a weird line, rather than a circle, gets drawn - FIXED.

Potential bug - when the table of data (inc. extrainfo) is too tall, it will simply get drawn over the edge of the image.  Ideally something to check and make the image taller is needed.

0.06 - backup of 0.05

0.07 (17-05-08)
-Fixed problems (hopefully) of when table of info is too tall/wide for image.
-Added more colours (now 7)
-vera font

(30-06-08)
-fixed divison by zero problems.
-fixed 'upside down' text being too close to pie.

(19/12/2008)
-no longer sets GDFONTPATH
const http = require('http');
var fs = require('fs');

app1Url = 'http://app1:3001'
app2Url = 'http://app2:3002'

function handleRequestError(err) {
  console.error(err);
  response.statusCode = 400;
  response.end();
}

function handleResponseError(err) {
  console.log(err)
}

function getText(request, response, appUrl) {
  return new Promise(function (resolve, reject) {
    httpGet(appUrl).then(r => {
      response.end(r)
      request.pipe(response)
      resolve()
    })
  })
}

function asdf(request, response, appUrl) {
    if (request.method === 'GET')
      if (request.url === '/') {
        getText(request, response, appUrl)
    
      }
    else {
      response.statusCode = 404;
      response.end();
    }
}

function processStuff(request, response) {
  
  let requestHost = request.headers.host
  
  if (requestHost.match('evabjewelry')) {
    asdf(request, response, app1Url)
  }
  if (requestHost.match('thenewcpu')) {
    asdf(request, response, app2Url)
  }
}

function httpGet(appUrl) {
  return new Promise(function (resolve, reject) {
    
    http.get(appUrl, (res) => {
      const { statusCode } = res;
      const contentType = res.headers['content-type'];
    
      let error;
      if (statusCode !== 200) {
        error = new Error('Request Failed.\n' +
                          `Status Code: ${statusCode}`);
      }
      
      if (error) {
        console.error(error.message);
        // consume response data to free up memory
        res.resume();
        return;
      }
    
      res.setEncoding('utf8');
      let rawData = '';
      res.on('data', (chunk) => { rawData += chunk; });
      res.on('end', () => {
        try {
          //const parsedData = JSON.parse(rawData);
          console.log(rawData);
          resolve(rawData)
          //return(rawData)
        } catch (e) {
          console.error(e.message);
          reject(e)
        }
      })
      
    }).on('error', (e) => {
      console.error(`Got error: ${e.message}`);
    });
  })
}

http.createServer((request, response) => {
  request.on('error', handleRequestError)
  response.on('error', handleResponseError)
  processStuff(request, response)
}).listen(8080);
# Load Balanced App
This example uses an NGINX container to act as a reverse proxy for 3 node.js apps.

## build, tear down
+ `$ docker-compose up`
+ `$ docker-compose down`
